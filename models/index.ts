import {Table, Column,  Model, HasMany} from 'sequelize-typescript';

@Table
class Novel extends Model<Novel> {
    @Column
    novelName: string;

    @Column
    authorName: string;

    @Column
    publishDate: Date;

    @Column
    volumes: number;

    @Column
    pictureUrl: Blob;
}