import { Sequelize } from 'sequelize-typescript';

const sequelize = new Sequelize({
    database : '',
    dialect : 'postgres',
    username : '',
    password : '',
    storage: ':memory:',
    modelPaths: ['']
});