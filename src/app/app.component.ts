import { Component } from '@angular/core';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as errorhandler from 'strong-error-handler';

// export const app = express();

// app.use(bodyParser.urlencoded({extended: true}));

// app.use(bodyParser.json({limit: '5mb'}));

// app.use((req, res, next) => {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Expose-Headers", "x-total-count");
//   res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
//   res.header("Access-Control-Allow-Headers", "Content-Type,authorization");

//   next();
// });

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// app.use(errorhandler({
//   debug: process.env.ENV !== 'prod',
//   log: true,
// }));

export class AppComponent {
  title = 'Manga Lister';
}

